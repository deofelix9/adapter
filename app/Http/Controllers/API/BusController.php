<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController as BaseController;
use DB;

class BusController extends BaseController
{

    // public function __construct()
    // {
    //   $this->middleware('auth:api');
    // }
    
    public function allBus()
    {
       
        $products = DB::table('dacina')->get();
        //dd($products);
        return $this->sendResponse($products->toArray(), 'Bus retrieved successfully.');
    }


    public function allBusthisStation($stopName,$direction)
    {
       //Return all buses which next stop name is equal to the commutter location
        $buses = DB::table('dacina')
        ->where('NEXT_STOP_NAME',$stopName)
        ->where('PREVIOUS_PASSING_TIME_ID',$direction) 
        ->get();
        
        //Hold the current location of the bus which its next stop is equal to the location of the commutter
        foreach ($buses as $bus){
            $heldCurrentStop=$bus->VEHICLE_JOURNEY_ID;
           // return $heldCurrentStop;
        }

    
      //Other Location of bus next to commutter appart from the nearest one
        $bus = DB::table('dacina')
        ->where('VEHICLE_JOURNEY_ID',$heldCurrentStop)
        ->where('PREVIOUS_PASSING_TIME_ID',$direction)
        ->get();

        $pr=$bus->merge($buses);
       
        // return view('bus')->with('products',$products)
        //                  ->with('product',$product);

       return $this->sendResponse($pr->toArray(), 'Bus retrieved successfully.');
      // dd($product->VEHICLE_JOURNEY_ID);
       
    }

    public function bus($id)
    {
        //dd($id);
        $product = DB::table('dacina')->where('NODE_NUMBER',$id)->first();

        //dd($product);
        if (is_null($product)) {
            return $this->sendError('Bus not found.');
        }


        return $this->sendResponse($product, 'Bus retrieved successfully.');
    }
}
